Este é um programa escrito em ruby para calcular o valor final de um pedido em um e-commerce.
Segue os passos abaixo para utilizá-lo.

É necessário ter instalado o Ruby e o Git em seu computador:

  - Para instalar o Ruby, basta entrar nessa URL: `https://www.ruby-lang.org/pt/documentation/installation/`, escolher o seu sistema operacional e seguir o tutorial.

  - Para instalar o Git, basta entrar nessa URL: `https://git-scm.com/downloads` e realizar o download de acordo com seu sistema operacional.

Clone o projeto: `git clone git@bitbucket.org:rodrigomageste/teste-luz.git`

Navege até a raiz do diretório do projeto clonado: `cd teste-luz/`

Para rodar os testes:
  - Basta executar o comando `rspec spec/`

Para executar o programa:

  - Vá para a raiz do diretório

  - Coloque os arquivos .csv dentro da pasta `csv_files/`

  - Executar o comando `./sum_up_orders`
    - Ex.: `./sum_up_orders coupons.csv products.csv orders.csv order_items.csv totals.csv`

  - O arquivo **totals.csv** será gerado no diretório `csv_files/`