require 'pry'
require './lib/handle_io.rb'
require './classes/order.rb'
require './classes/order_items.rb'
require './classes/coupon.rb'
require './classes/product.rb'

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.before(:each) do
    Object.send(:remove_const, 'Coupon')
    load './classes/coupon.rb'

    Object.send(:remove_const, 'Product')
    load './classes/product.rb'

    Object.send(:remove_const, 'Order')
    load './classes/order.rb'

    Object.send(:remove_const, 'OrderItems')
    load './classes/order_items.rb'
  end
end
