RSpec.describe Order do
  describe '#initialize' do
    before do
      @order = Order.new(['234','123'])
    end

    it 'assign integer id' do
      expect(@order.id).to eq(234)
    end

    it 'assign integer coupon_id' do
      expect(@order.coupon_id).to eq(123)
    end
  end

  describe '.all' do
    it 'return all coupons' do
      order1 = Order.new(['234','123'])
      order2 = Order.new(['345','234'])
      expect(Order.all).to include(order1, order2)
    end
  end

  describe '.sum_up_and_build_csv' do
    before do
      Coupon.build_objects_from('coupons.csv')
      Product.build_objects_from('products.csv')
      Order.build_objects_from('orders.csv')
      OrderItems.build_objects_from('order_items.csv')
      @sum_up_orders = Order.sum_up_and_build_csv('sum_up_orders.csv')
      File.delete("./csv_files/sum_up_orders.csv")
    end

    it 'return all orders' do
      expect(@sum_up_orders.count).to eq(6)
    end

    it 'return a vector contain order_id and value of the order' do
      expect(@sum_up_orders.first[0]).to eq(123)
      expect(@sum_up_orders.first[1]).to eq(2250.0)
    end
  end

  describe '#sum_up' do
    it 'return array with order sum_up' do
      Product.new(['789','150.0'])
      Product.new(['987','150.0'])
      OrderItems.new(['123','789'])
      OrderItems.new(['123','987'])
      order = Order.new(['123','234'])
      expect(order.sum_up).to eq([123, 270])
    end
  end

  describe '#coupon' do
    it 'return correct coupon' do
      coupon = Coupon.new(['123', '25', 'absolute', '2020/12/25', '1'])
      order = Order.new(['234','123'])
      expect(order.coupon).to eq(coupon)
    end

    it 'return nil when coupon not exist' do
      order = Order.new(['234','123'])
      expect(order.coupon).to eq(nil)
    end

    it 'return nil when order not has bind a coupon' do
      order = Order.new(['234', nil])
      expect(order.coupon).to eq(nil)
    end
  end

  describe '#products' do
    it 'return all products belong to order' do
      product1 = Product.new(['789','150.0'])
      product2 = Product.new(['987','150.0'])
      order = Order.new(['123','234'])
      OrderItems.new(['123','789'])
      OrderItems.new(['123','987'])
      expect(order.products).to include(product1, product2)
    end

    it 'return nil when order not contain products' do
      order = Order.new(['234','123'])
      expect(order.products).to eq([])
    end
  end

  describe '#sum_up_products' do
    it 'sum all products price' do
      Product.new(['789','150.0'])
      Product.new(['987','150.0'])
      OrderItems.new(['123','789'])
      OrderItems.new(['123','987'])
      order = Order.new(['123','234'])
      expect(order.sum_up_products).to eq(300.0)
    end
  end

  describe '#progressive_discount' do
    it 'number of products is less than or equal 1' do
      Product.new(['789','150.0'])
      OrderItems.new(['123','789'])
      order = Order.new(['123','234'])

      expect(order.progressive_discount).to eq(0)
    end

    it 'number of products is greater than 1 and less than 9' do
      Product.new(['789','150.0'])
      6.times do
        OrderItems.new(['123','789'])
      end
      order = Order.new(['123','234'])

      expect(order.progressive_discount.round(2)).to eq(0.30)
    end

    it 'number of products is greater than 8' do
      Product.new(['789','150.0'])
      9.times do
        OrderItems.new(['123','789'])
      end
      order = Order.new(['123','234'])

      expect(order.progressive_discount).to eq(0.40)
    end
  end

  describe '#apply_discount' do
    context 'when cupon discount' do
      it 'invalid' do
        Coupon.new(['125', '25', 'absolute', '2015/12/25', '1'])
        Product.new(['789','150.0'])
        OrderItems.new(['123','789'])
        order = Order.new(['123','125'])
        expect(order.apply_discount).to eq(150.0)
      end

      it 'valid' do
        Coupon.new(['125', '25', 'absolute', '2017/12/25', '1'])
        Product.new(['789','150.0'])
        OrderItems.new(['123','789'])
        order = Order.new(['123','125'])
        expect(order.apply_discount).to eq(125.0)
      end
    end

    it 'order not binding a cupon' do
      Coupon.new(['125', '25', 'absolute', '2017/12/25', '1'])
      Product.new(['789','150.0'])
      OrderItems.new(['123','789'])
      order = Order.new(['123', nil])
      expect(order.apply_discount).to eq(150.0)
    end
  end
end
