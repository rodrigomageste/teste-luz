RSpec.describe Coupon do
  describe '#initialize' do
    before do
      @coupon = Coupon.new(['123', '25', 'absolute', '2020/12/25', '1'])
    end

    it 'assign integer id' do
      expect(@coupon.id).to eq(123)
    end

    it 'assign float discount' do
      expect(@coupon.discount).to eq(25.0)
    end

    it 'assign string discount_type' do
      expect(@coupon.discount_type).to eq('absolute')
    end

    it 'assign date due_date' do
      expect(@coupon.due_date.to_s).to eq('2020-12-25')
    end

    it 'assign integer times_to_use' do
      expect(@coupon.times_to_use).to eq(1)
    end
  end

  describe '.all' do
    it 'return all coupons' do
      coupon1 = Coupon.new(['123', '25', 'absolute', '2020/12/25', '1'])
      coupon2 = Coupon.new(['125', '25', 'absolute', '2020/12/25', '1'])
      expect(Coupon.all).to include(coupon1, coupon2)
    end
  end

  describe '#get_discount' do
    it 'when discount is absolute' do
      coupon = Coupon.new(['123', '25', 'absolute', '2020/12/25', '1'])
      expect(coupon.get_discount(100)).to eq(0.25)
    end

    it 'when discount is percentual' do
      coupon = Coupon.new(['123', '25', 'percentual', '2020/12/25', '1'])
      expect(coupon.get_discount(200)).to eq(0.25)
    end

    it 'when discount is not valid' do
      coupon = Coupon.new(['123', '25', 'percentual', '2020/12/25', '0'])
      expect(coupon.get_discount(200)).to eq(0)
    end
  end

  describe '#is_valid?' do
    context 'when due_date' do
      it 'is not overdue' do
        coupon = Coupon.new(['123', '25', 'absolute', '2018/12/25', '1'])
        expect(coupon.is_valid?).to be_truthy
      end

      it 'is overdue' do
        coupon = Coupon.new(['123', '25', 'absolute', '2015/12/25', '1'])
        expect(coupon.is_valid?).to be_falsey
      end
    end

    context 'when times_to_use' do
      it 'is not sould out' do
        coupon = Coupon.new(['123', '25', 'absolute', '2018/12/25', '1'])
        expect(coupon.is_valid?).to be_truthy
      end

      it 'is sould out' do
        coupon = Coupon.new(['123', '25', 'absolute', '2018/12/25', '0'])
        expect(coupon.is_valid?).to be_falsey
      end
    end
  end
end
