RSpec.describe Product do
  describe '#initialize' do
    before do
      @product = Product.new(['123','150.0'])
    end

    it 'assign integer id' do
      expect(@product.id).to eq(123)
    end

    it 'assign float price' do
      expect(@product.price).to eq(150.0)
    end
  end

  describe '.all' do
    it 'return all coupons' do
      product1 = Product.new(['123','150.0'])
      product2 = Product.new(['125','150.0'])
      expect(Product.all).to include(product1, product2)
    end
  end
end
