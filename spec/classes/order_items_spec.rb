RSpec.describe OrderItems do
  describe '#initialize' do
    before do
      @order_item = OrderItems.new(['123','789'])
    end

    it 'assign integer order_id' do
      expect(@order_item.order_id).to eq(123)
    end

    it 'assign float product_id' do
      expect(@order_item.product_id).to eq(789)
    end
  end

  describe '.all' do
    it 'return all coupons' do
      order_item1 = OrderItems.new(['123','789'])
      order_item2 = OrderItems.new(['123','987'])
      order_item3 = OrderItems.new(['234','678'])
      expect(OrderItems.all).to include(order_item1, order_item2, order_item3)
    end
  end

  describe '#product' do
    it 'return product' do
      product = Product.new(['789','150.0'])
      order_item = OrderItems.new(['123','789'])
      expect(order_item.product).to eq(product)
    end
  end

  describe '#order' do
    it 'return order' do
      order = Order.new(['123','150.0'])
      order_item = OrderItems.new(['123','789'])
      expect(order_item.order).to eq(order)
    end
  end
end
