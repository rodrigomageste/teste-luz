RSpec.describe HandleIO do
  describe '.build_objects_from' do
    it 'build coupons' do
      coupons = Coupon.build_objects_from('coupons.csv')
      expect(coupons.count).to eq(5)
    end

    it 'build orders' do
      coupons = Order.build_objects_from('orders.csv')
      expect(coupons.count).to eq(6)
    end

    it 'build products' do
      coupons = Order.build_objects_from('products.csv')
      expect(coupons.count).to eq(10)
    end

    it 'build order_items' do
      coupons = OrderItems.build_objects_from('order_items.csv')
      expect(coupons.count).to eq(17)
    end
  end

  describe '.build_file' do
    it 'generate file csv' do
      Object.new.extend(HandleIO).build_file([[123,324], [123,322]], 'output')
      boolean_output = File.exist?("./csv_files/output")
      File.delete("./csv_files/output")
      expect(boolean_output).to eq(true)
    end
  end
end
