require 'csv.rb'

module HandleIO
  def build_objects_from(file)
    @objects = []
    CSV.foreach("./csv_files/#{file}") do |row|
      next if row.compact.empty?
      @objects << self.new(row)
    end
    @objects
  end

  def build_file(vector, output_file_name)
    CSV.open("./csv_files/#{output_file_name}", "wb") do |csv|
      vector.each { |element| csv << element }
    end
  end
end
