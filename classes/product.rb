require './lib/handle_io.rb'

class Product
  extend HandleIO
  @@all = Array.new
  attr_reader :id, :price

  def initialize(csv_line)
    @id = csv_line[0].to_i
    @price = csv_line[1].to_f
    @@all << self
  end

  def self.all
    @@all
  end
end
