require './lib/handle_io.rb'
require './classes/order.rb'
require './classes/order_items.rb'
require './classes/product.rb'

class OrderItems
  extend HandleIO
  @@all = Array.new
  attr_reader :order_id, :product_id

  def initialize(csv_line)
    @order_id = csv_line[0].to_i
    @product_id = csv_line[1].to_i
    @@all << self
  end

  def self.all
    @@all
  end

  def product
    Product.all.select { |product| product.id == product_id }.first
  end

  def order
    Order.all.select { |order| order.id == order_id }.first
  end
end
