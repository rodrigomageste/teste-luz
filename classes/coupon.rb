require './lib/handle_io.rb'

class Coupon
  extend HandleIO
  @@all = Array.new
  attr_reader :id, :discount, :discount_type, :due_date, :times_to_use

  def initialize(csv_line)
    @id = csv_line[0].to_i
    @discount = csv_line[1].to_f
    @discount_type = csv_line[2]
    @due_date = Date.strptime(csv_line[3], "%Y/%m/%d")
    @times_to_use = csv_line[4].to_i
    @@all << self
  end

  def self.all
    @@all
  end

  def get_discount(total_amount)
    return 0 unless is_valid?

    if discount_type == 'absolute'
      discount / total_amount
    else
      discount / 100
    end
  end

  def is_valid?
    return (Date.today < due_date) && (times_to_use > 0)
  end
end
