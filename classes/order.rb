require './lib/handle_io.rb'
require './classes/coupon.rb'

class Order
  extend HandleIO
  @@all = Array.new
  attr_reader :id, :coupon_id

  def initialize(csv_line)
    @id = csv_line[0].to_i
    @coupon_id = csv_line[1].to_i
    @@all << self
  end

  def coupon
    Coupon.all.select { |coupon| coupon.id == coupon_id }.first
  end

  def products
    OrderItems.all.select { |order_item| order_item.order_id == id }
      .map { |order_item| order_item.product }.compact
  end

  def sum_up_products
    products.map(&:price).reduce(:+)
  end

  def apply_discount
    total_amount = sum_up_products
    coupon_discount = coupon ? coupon.get_discount(total_amount) : 0

    discount = coupon_discount > progressive_discount ? coupon_discount : progressive_discount
    total_amount * (1 - discount)
  end

  def progressive_discount
    number_of_products = products.count
    case number_of_products
    when 0..1
      0
    when 2..8
      (number_of_products - 2) * 0.05 + 0.1
    else
      0.4
    end
  end

  def sum_up
    [id, apply_discount]
  end

  def self.sum_up_and_build_csv(file_name)
    sum_up_orders = Order.all.map { |order| order.sum_up }
    Order.build_file(sum_up_orders, file_name)
  end

  def self.all
    @@all
  end
end
